BWA Index GeneFlow App
======================

Version: 0.7.17-02

This GeneFlow2 app wraps the BWA Index tool.

Inputs
------

1. reference: Reference Sequence FASTA - A reference sequence in FASTA file format to be indexed for BWA. 

Parameters
----------

1. output: Output Directory - The directory at which the new BWA index will be generated. The default value for the output directory is "reference". This directory is relative to the workflow or step output location. 
